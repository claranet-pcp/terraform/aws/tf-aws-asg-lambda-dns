# SNS topic
resource "aws_sns_topic" "dns" {
  count = var.enabled
  name = var.sns_topic_name
}

# SNS subscription
resource "aws_sns_topic_subscription" "sns_topic_subscription" {
  count     = var.enabled
  topic_arn = aws_sns_topic.dns[0].arn
  protocol  = "lambda"
  endpoint  = module.lambda.function_arn
}

