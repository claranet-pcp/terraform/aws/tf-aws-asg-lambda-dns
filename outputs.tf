output "lambda_manage_dns_role_arn" {
  value = join("", module.lambda.*.role_arn)
}

output "lambda_function_arn" {
  value = join("", module.lambda.*.function_arn)
}

output "lambda_function_name" {
  value = join("", module.lambda.*.function_name)
}

output "sns_topic_arn" {
  value = join("", aws_sns_topic.dns.*.arn)
}

