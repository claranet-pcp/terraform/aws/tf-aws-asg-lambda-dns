# Lambda policy
data "aws_iam_policy_document" "lambda" {
  count = var.enabled

  statement {
    actions = [
      "route53:GetHostedZone",
      "route53:ChangeResourceRecordSets",
      "route53:ListResourceRecordSets",
    ]

    resources = [
      "arn:aws:route53:::hostedzone/${var.zone_id}",
    ]
  }

  statement {
    actions = [
      "autoscaling:DescribeAutoScalingGroups",
      "autoscaling:DescribeAutoScalingInstances",
    ]

    resources = [
      "*",
    ]
  }

  statement {
    actions = [
      "ec2:DescribeInstances",
    ]

    resources = [
      "*",
    ]
  }

  dynamic "statement" {
    # The assumerole statement is only required if dns_role_arn is populated.
    for_each = var.dns_role_arn != "" ? [1] : []

    content {
      actions = [
        "sts:AssumeRole",
      ]

      resources = [
        var.dns_role_arn,
      ]
    }
  }
}
